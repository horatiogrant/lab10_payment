
package Big;

import java.util.ArrayList;



public class Cart {
  
   PaymentServiceFactory factory = PaymentServiceFactory.makeFactory(); 
   private ArrayList <Product> products = new ArrayList<Product>();
   private PaymentService service;
   
   public void setPaymentService(PaymentService service){
       this.service=service;
   }
   
   public void addProduct(Product product){
       products.add(product);
       
   }
   
   public void payCart(){
       
       double total=0;
       for(int x=0;x<products.size();x++){
           total+=products.get(x).getPrice();
       }
      service.processPayment(total);
       
   }
    
}
