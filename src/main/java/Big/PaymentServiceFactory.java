
package Big;


public class PaymentServiceFactory {
    public static PaymentServiceFactory paymentFactory;
    
    private PaymentServiceFactory(){}
    
    public static PaymentServiceFactory makeFactory(){
    
    if(paymentFactory==null){
        paymentFactory = new PaymentServiceFactory();
    }
    return paymentFactory;
}
    
   public PaymentService getPaymentService(PaymentServiceType type){
        
       if(type==PaymentServiceType.CREDIT){
              return new CreditPayment() ;
          }else{
            return new DebitPayment() ;
       }
      
    }
    
}
