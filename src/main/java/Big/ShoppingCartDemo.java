
package Big;


public class ShoppingCartDemo {
    
    public static void main(String [] args){
      PaymentServiceFactory factory = PaymentServiceFactory.makeFactory();
      PaymentService creditService = factory.getPaymentService(PaymentServiceType.CREDIT);
      PaymentService debitService = factory.getPaymentService(PaymentServiceType.DEBIT);  
        Cart cart = new Cart();
        
        
        cart.addProduct(new Product("shirt",50));
        cart.addProduct(new Product("pants",60));
        
        cart.setPaymentService(debitService);
        cart.payCart();
    }
    
}
