
package lab10.lab10;

import java.util.Calendar;

public class Tool {
    
    public boolean isPromotionDueThisYear(Employee employee, boolean goodPerformance){
        Calendar datejoined = Calendar.getInstance();
        datejoined.setTime(employee.getEmployeeDateOfJoining());
        datejoined.add( Calendar.YEAR, 1);
        
        Calendar today = Calendar.getInstance();
        
      
        return today.after(datejoined)&&goodPerformance;
    }
    
    public double calculateTaxForCurrentYear(Employee emp){
        
        return emp.getWage()*0.3;
    }
    
}
