/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shape;

public class Rectangle {
    protected double height;
     protected double width;
     
     
     public double getArea(){
         return height*width;
     }
     public double getPerimiter(){
         return (2*height)+(2*width);
     }
    
      public void setHeight(Double height){
        this.height=height;
   
    }
    public void setWidth(Double width){
       
        this.width=width;
    }
}
