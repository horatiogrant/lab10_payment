/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shape;

public class driver {
    
    public static void calculateArea(Rectangle r){
        r.setWidth( 2.0 );
         r.setHeight( 3.0 );
         
         if (r.getArea()!=6){
         System.out.println("Area calculation is incorrect");
         }else{
              System.out.println("GOOD");
         }
    }
    
    public static void main(String[]args){
        driver.calculateArea(new Rectangle());
        
        driver.calculateArea(new Square());
    }
    
}
